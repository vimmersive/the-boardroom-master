﻿using UnityEngine;
using System.Collections;

public class Rise : MonoBehaviour
{
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime);
    }
}