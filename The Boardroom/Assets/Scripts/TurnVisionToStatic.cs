﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Colorful;

public class TurnVisionToStatic : MonoBehaviour
{
    public float noiseValue;
    public Noise[] noiseComponents;
    public CanvasGroup white;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        white.alpha = noiseValue;

        for (int i = 0; i < noiseComponents.Length; i++)
        {
            noiseComponents[0].Strength = Mathf.Lerp(0, 1, noiseValue);
            noiseComponents[1].Strength = Mathf.Lerp(0, 1, noiseValue);
            noiseComponents[2].Strength = Mathf.Lerp(0, 1, noiseValue);
        }
    }
}
