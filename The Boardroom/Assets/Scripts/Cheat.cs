﻿using UnityEngine;
using System.Collections;

public class Cheat : MonoBehaviour {
	int index = 0;
	public GameObject one;
	public GameObject two;
	public GameObject three;
	
	private TurnVisionToStatic staticStuff;

	void Start()
	{
		staticStuff = FindObjectOfType<TurnVisionToStatic>();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			index++;
			if (index == 1)
			{
				one.SetActive(false);
				two.SetActive(true);
				three.SetActive(false);
			}
			else if (index == 2)
			{
				one.SetActive(false);
				two.SetActive(false);
				three.SetActive(true);
				StartCoroutine(TurnStaticOff());
			}
		}
	}

	IEnumerator TurnStaticOff()
	{
		float myValur = staticStuff.noiseValue;

		for (float i = 0; i < 1; i+= Time.deltaTime / 3f)
		{
			staticStuff.noiseValue = Mathf.Lerp(myValur, 0, i);
			yield return null;
		}
	}

	}
