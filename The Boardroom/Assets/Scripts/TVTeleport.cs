﻿using UnityEngine;
using System.Collections;

public class TVTeleport : MonoBehaviour
{
    public float startDist = 10;
    public float endDist = 2;

    private TurnVisionToStatic noiseFilter;

    // Use this for initialization
    void Start()
    {
        noiseFilter = FindObjectOfType<TurnVisionToStatic>();
    }

    // Update is called once per frame
    void Update()
    {
        float distFromCam = (Camera.main.transform.position - transform.position).magnitude;

        if (distFromCam < startDist)
        {
            noiseFilter.noiseValue = Mathf.Lerp(1, 0, (distFromCam - endDist) / (startDist - endDist));
        }
    }
}
