﻿using UnityEngine;
using System.Collections;

public class ColorChange : MonoBehaviour
{
    public Renderer renderer;

    public void TriggerMe(string color)
    {
        renderer.material.color = color.ToColor();
    }
}
