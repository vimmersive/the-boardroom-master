﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class PaperTeleport : MonoBehaviour
{
    public BoxCollider ownBound;
    public BoxCollider[] outerBounds;
    public UnityEvent triggerEvents;

    // Update is called once per frame
    void Update()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        bool seeOwnBound = GeometryUtility.TestPlanesAABB(planes, ownBound.bounds);
        bool seeOuterBound = false;

        foreach (var item in outerBounds)
        {
            if (GeometryUtility.TestPlanesAABB(planes, item.bounds))
            {
                seeOuterBound = true;
                break;
            }
        }
        Debug.Log(string.Format("{0}, {1}", seeOwnBound, seeOuterBound));
        
        if (seeOwnBound && !seeOuterBound)
        {
            Debug.Log("Teleport!");
            triggerEvents.Invoke();
        }
    }
}
