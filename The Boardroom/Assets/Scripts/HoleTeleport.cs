﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class HoleTeleport : MonoBehaviour
{
    public float targetAlpha = 1;
    public float transitionSeconds = 2f;
    public CanvasGroup canvasGroup;
    public string sceneName;

    void Start()
    {
        Destroy(canvasGroup.GetComponent<Animator>(), 3);
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            StartCoroutine(LerpDarkness());
        }
    }

    IEnumerator LerpDarkness()
    {
        float origAlpha = canvasGroup.alpha;

        for (float i = 0; i < 1; i += Time.deltaTime / transitionSeconds)
        {
            canvasGroup.alpha = Mathf.Lerp(origAlpha, targetAlpha, Mathf.SmoothStep(0, 1, i));
            yield return null;
        }

        SceneManager.LoadScene(sceneName);
    }
}
